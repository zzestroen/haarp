﻿	$('#content').append('<div id="panel_disqus" style="display: none; "></div><p id="slider_disqus" class="slide"><a href="#" class="btn-slide">Комментарии</a></p>');
	$("#slider_disqus .btn-slide").click(function()
	{
		if($("#disqus_thread").length<1)
		{
			var disqusContainer = $('#panel_disqus');
			disqusContainer.append("<div id=\"disqus_thread\"></div>");
			disqusContainer.append('<div id="disqus">');
			/* * * CONFIGURATION VARIABLES: EDIT BEFORE PASTING INTO YOUR WEBPAGE * * */
			var disqus_shortname = 'haarp'; // required: replace example with your forum shortname
			var disqus_developer = 1;
			var disqus_identifier = 'test';
			/* * * DON'T EDIT BELOW THIS LINE * * */
			(function() {
				var dsq = document.createElement('script'); dsq.type = 'text/javascript'; dsq.async = true;
				dsq.src = 'http://' + disqus_shortname + '.disqus.com/embed.js';
				(document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(dsq);
			})();

			$('body').append("<noscript>Please enable JavaScript to view the <a href=\"http://disqus.com/?ref_noscript\">comments powered by Disqus.</a></noscript>\
			<a href=\"http://disqus.com\" class=\"dsq-brlink\">blog comments powered by <span class=\"logo-disqus\">Disqus</span></a>");
			/* * * DISQUS END * * */
		}
		$("#panel_disqus").slideToggle("slow");
		$(this).toggleClass("active"); 
		return false;
	});