//модуль опеспечивающий работу с HTML5 Web SQL Database
//Максимальный рекомендуемый для браузеров размер базы данных 5Мб.
//При запросе большей базы данных, браузер должен выдавать сообщение пользователю с предупреждением об опасности и просьбой подтвердить большой размер базы данных.

//менеджер базы данных
var dbManager = (function (user, list)
{
    //элемент для вывода таблицы
    var elm = document.querySelector(list),
        //элемент для вывода статуса
        //внутренняя переменная базы
        db = null,
        showingTimeline = true,
        latest = 0,
        dbName = "haarp.db",
        dbVersion = "0.2",
        dbDiscription = "Hockey Arena Analytical Research Plugin Offline Database",
        dbEstimatedSize = 100 * 1024 * 1024,
		Qimg = "img[src='http://www.hockeyarena.net/pics/target.gif']",
		Qimgs = 'http://www.hockeyarena.net/pics/target.gif'
		;

    //инициализация базы
    function create_tables() {
        console.log('Подготовка базы');
        try 
		{
            if (window.openDatabase) {
                db = openDatabase(dbName, dbVersion, dbDiscription, dbEstimatedSize);
                if (db) {
                    db.transaction(function (tx,e)
					{
                        var queryArray = [];
                        queryArray.push("CREATE TABLE IF NOT EXISTS history_public_player_info (record_id integer PRIMARY KEY AUTOINCREMENT, player_id integer, name TEXT, age integer, country TEXT,contract integer,team_id integer,injury integer,performance integer,performanceQuestion integer,potential integer,potentialQuestion integer,ability_index integer,satisfaction integer,weeks_in_team integer,add_date_time DATETIME);");
                        queryArray.push("CREATE TABLE IF NOT EXISTS history_manager_training_form1 (record_id integer PRIMARY KEY AUTOINCREMENT, player_id integer, name TEXT, age integer, goa integer, def integer, att integer, sho integer, pas integer, spe integer, str integer, sc integer, ene integer, form integer, tra integer, schedule text, pos text ,add_date_time DATETIME);");
                        queryArray.push("CREATE TABLE IF NOT EXISTS history_manager_team_players (record_id integer PRIMARY KEY AUTOINCREMENT, player_id integer, name TEXT, age integer, perf integer, perfq integer, pot integer, potq integer, skill integer, ai integer, wit integer, add_date_time DATETIME);");
                        queryArray.push("CREATE TABLE IF NOT EXISTS history_manager_team_contracts (record_id integer PRIMARY KEY AUTOINCREMENT, player_id integer, name TEXT, age integer, contract integer, salary integer, perf integer, perfq integer, pot integer, potq integer, loy integer, countryShort text, ai integer, add_date_time DATETIME);");
                        queryArray.push("CREATE TABLE IF NOT EXISTS history_manager_player_market_sql (record_id integer PRIMARY KEY AUTOINCREMENT, player_id integer, name TEXT, age integer, goa integer, def integer, att integer, sho integer, pas integer, spe integer, str integer, sc integer, ai integer, skill integer, perf integer, perfq integer, pot integer, potq integer, contract text, nat text, price integer, deadline DATETIME, add_date_time DATETIME);");
                        queryArray.push("CREATE TABLE IF NOT EXISTS history_manager_transfer_list_sql (record_id integer PRIMARY KEY AUTOINCREMENT, player_id integer, name TEXT, day TEXT, price integer, team_from_name TEXT, team_from_id integer, man_from_name TEXT, man_from_id integer, team_to_name TEXT, team_to_id integer, man_to_name TEXT, man_to_id integer, add_date_time DATETIME);");
                        queryArray.push("CREATE TABLE IF NOT EXISTS history_daily (record_id integer PRIMARY KEY AUTOINCREMENT, page_name TEXT, add_date_time DATETIME);");
                        queryArray.push("CREATE TABLE IF NOT EXISTS history_public_league_standings (record_id integer PRIMARY KEY AUTOINCREMENT, league_name TEXT, position integer, team_id integer, team_name TEXT, mat integer, win integer, drw integer, lose integer, wo integer, lo integer, pts integer, g_p integer, g_n integer, pn integer, home TEXT, away TEXT, add_date_time DATETIME);");
                        for (var i = 0; i < queryArray.length; i++) {
                            //notice(queryArray[i]);
                            if ((queryArray[i]) != undefined) tx.executeSql(queryArray[i], [], function (tx, result) {
                                //notice('inserted');
                            }, function (tx,e) {
                                error_notice('Ошибка при создании таблицы: код ' +e.code + ": " + e.message);
                            });
                            //notice(queryArray[i]);
                        }
						window.queryArray = null;
                    });
				} else {error_notice('Ошибка при открытии базы');}
            } else {error_notice('Web Databases не поддерживается');}
        } catch (e) {error_notice('Ошибка! '+e.name + ": " + e.message);}
		return db;
    }

	//возвращает имя файла из пути/имени js
	function hname(href)
	{
		return href.slice(href.lastIndexOf('/') + 1);
	}
	
		//возвращает имя файла из пути/имени js
	function notice(notice_text, time)
	{
 		console.log(notice_text);
		if(undefined==time)
			time=5000;
		noty({
			"text": notice_text,
			"theme": "noty_theme_twitter",
			"layout": "bottomLeft",
			"type": "information",
			"animateOpen": {"height":"toggle","opacity":"show"},
			"animateClose": {"height":"toggle","opacity":"hide"},
			"speed": 1000,
			"timeout": time,
			"closeButton": true,
			"closeOnSelfClick": true,
			"closeOnSelfOver": true,
			"modal": false
		});
	}
	
	function error_notice(notice_text)
	{
		console.log(notice_text);
		
		noty({
			"text": notice_text,
			"theme": "noty_theme_twitter",
			"layout": "bottomLeft",
			"type": "error",
			"animateOpen": {"height":"toggle","opacity":"show"},
			"animateClose": {"height":"toggle","opacity":"hide"},
			"speed": 300,
			"timeout": false,
			"closeButton": true,
			"closeOnSelfClick": true,
			"closeOnSelfOver": false,
			"modal": false
		});
	}

    //функция получает #page со страницы игрока
    //и из него получает нужные данные для вставки в таблицу
    function insert_history_public_player_info(page) {
        try {
			if($.trim($(page).find("table tbody tr").text())!="Игрок отсутствует или закончил карьеру !")
			if($.trim($(page).find("table tbody tr").text())!="Wrong player_id"){
			if (window.openDatabase) {
				//db = openDatabase(dbName, dbVersion, dbDiscription, dbEstimatedSize);
				if (db) {
					var trs = $(page).find("table tbody tr");
					var player_id = $.trim($(trs[0]).text()).split(",")[1].substr(4);
					var name = ($.trim($(trs[0]).text()).split(",")[0].split("игроке ")[1]);
					var age = $($(trs[1]).find(".q")[0]).text();
					var country = $.trim($($(trs[1]).find(".q")[1]).text())
					var contract = $($(trs[2]).find(".q")[0]).text().split(" ")[0];

					var team_id = getUrlVars($(trs[2]).find("a").attr("href"))['team_id'];
					var injury = 0;
					if ($($(trs[3]).find(".q")[0]).text()!="Здоров")
						var injury = $($(trs[3]).find(".q")[0]).text().split('(')[1].split(' ')[0];

					var perf = $.trim($($(trs[3]).find(".q")[1]).text()).replace('%', '');
					var perfQ = 0;
					if($($(trs[3]).find(".q")[1]).find("img").attr("src")==Qimgs)
						perfQ=1;

					var pot = $.trim($($(trs[4]).find(".q")[1]).text()).replace('%', '')
					var potQ = 0;
					if($($(trs[4]).find(".q")[1]).find("img").attr("src")==Qimgs)
						potQ=1;

					var ai = $.trim($($(trs[5]).find(".q")[0]).text());

					var satis = $.trim($.trim($($(trs[6]).find(".q")[0]).text()).split("(")[1].split(")")[0].replace('=', ''));
					var wit = $.trim($($(trs[6]).find(".q")[1]).text());

					//проверка на допустимость значений
					if (player_id < 1)
						error_notice("ошибка player_id");
					
					var queryArray =
					[player_id, //номер игрока
					name, //имя
					age, //возраст
					country, //страна
					contract, //остаток контракта
					team_id, //команда
					injury, //дней травмы
					perf, //работоспособность
					perfQ, //работоспособность приблизительна(0) или точна(1)
					pot, //потенциал
					potQ, //потенциал приблизителен(0) или точен(1)
					ai, //сумма умений
					satis, //удовлетворённость
					wit //Недель в команде
					];
					db.transaction(function (tx,e)
					{
						tx.executeSql("INSERT INTO history_public_player_info VALUES(NULL, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, datetime('now'));", queryArray, function (tx, result)
						{
							if(undefined!=player_id)
								insert_history_daily(tx,"history_public_player_info_"+player_id);
							//notice("inserted history_public_player_info");
						}, function (tx,e) {
							error_notice('Ошибка при вставке в базу:'+ tx + " код " +e.code + ": " + e.message);
							});
					});
				} else {error_notice('Ошибка при открытии базы');}
            } else {error_notice('Web Databases не поддерживается');}}
        } catch (e) {error_notice('Ошибка подготовки базы, Web Database подерживается? '+e.name + ": " + e.message + "; id=" + $($(page).find("table tbody tr")[0]).text());}
    }

	function fnAddPlayers_table(row)
	{
        var perf = "";
        if (row.performanceQuestion < 1) perf = "" + row.performance + "<img src=\"http://www.hockeyarena.net/pics/question.gif\">";
        else perf = "" + row.performance + "<img src=\"http://www.hockeyarena.net/pics/target.gif\">";
        var pot = "";
        if (row.potentialQuestion < 1) pot = "" + row.potential + "<img src=\"http://www.hockeyarena.net/pics/question.gif\">";
        else pot = "" + row.potential + "<img src=\"http://www.hockeyarena.net/pics/target.gif\">";
        
		return ["<a href=\"index.php?p=public_player_info.inc&amp;id=" + row.player_id + "\">" + row.name + "</a>", row.age, row.country, row.contract, row.team_id, row.injury, perf, pot, row.ability_index, row.satisfaction, row.weeks_in_team, row.add_date_time];
    }

    function selectPlayerPublic() {
        notice("Загрузка данных в таблицу");
        db.transaction(function (tx,e) {
            tx.executeSql('SELECT * FROM history_public_player_info GROUP BY player_id ORDER BY add_date_time', [], function (tx, results)
			{
                if (results.rows && results.rows.length) {
                    notice('Загружено ' + results.rows.length + ' строк из базы');
                    var tr, trNode;
					var list = [];
                    for (i = 0; i < results.rows.length; i++) 
					{
                        list.push(fnAddPlayers_table(results.rows.item(i)));
					}
					$('#players_table').dataTable().fnAddData(list);
                }
				results = '';
            }, function (tx,e) {
                error_notice('Ошибка при выборке из базы:'+ tx + " код " +e.code + ": " + e.message);
            });
        });
    }
	
	function get_date_value(row)
	{
		var r = row.date.split("-");
		return [Date.UTC(r[0],  r[1]*1-1, r[2]),row.value];
    }
	
	//select_highcharts_public_player_info(player_id);
	function select_highcharts_public_player_info(player_id, tag) 
	{
		var result_data=[];
        db.transaction(function (tx,e) {
			var table_name;
			var tables =
			{
				"ability_index": "history_public_player_info",
				"goa": "history_manager_training_form1",
				"spe": "history_manager_training_form1",
				"def": "history_manager_training_form1",
				"str": "history_manager_training_form1",
				"att": "history_manager_training_form1",
				"sc": "history_manager_training_form1",
				"sho": "history_manager_training_form1",
				"form": "history_manager_training_form1",
				"pas": "history_manager_training_form1",
				"ene": "history_manager_training_form1",
				"tra": "history_manager_training_form1"
			};
			var tag_rus_full =
			{
				"ability_index": "График изменения Суммы Умений игрока",
				"goa": "График изменения навыка вратаря игрока",
				"spe": "График изменения навыка скорости игрока",
				"def": "График изменения навыка защиты игрока",
				"str": "График изменения навыка силы игрока",
				"att": "График изменения навыка нападения игрока",
				"sc": "График изменения навыка самообладания игрока",
				"sho": "График изменения навыка броска игрока",
				"form": "График изменения навыка формы игрока",
				"pas": "График изменения навыка паса игрока",
				"ene": "График изменения Энергии игрока",
				"tra": "График изменения Тренировок игрока"
			};
			var tag_rus_short =
			{
				"ability_index": "Сумма Умений",
				"goa": "Навык вратаря",
				"spe": "Навык скорости",
				"def": "Навык защиты",
				"str": "Навык силы",
				"att": "Навык нападения",
				"sc": "Навык самообладания",
				"sho": "Навык броска",
				"form": "Навык формы",
				"pas": "Навык паса",
				"ene": "Энергия",
				"tra": "Тренировки"
			};
			table_name=tables[tag];
			if(table_name!=undefined)
				tx.executeSql('select date(add_date_time) as date,'+tag+' as value from '+table_name+' where "player_id"=? group by date("add_date_time","+7 hour","+30 minute")', [player_id], function (tx, results)
				{
					if (results.rows && results.rows.length) {
						//notice('loading ' + results.rows.length + ' from DB');
						var tr, trNode;
						for (i = 0; i < results.rows.length; i++) 
						{
							result_data.push(get_date_value(results.rows.item(i)));
						}
					}
					
					var chart = new Highcharts.Chart({
						chart: {
							renderTo: 'container'+tag,
							type: 'spline'
						},
						title: {text: tag_rus_full[tag]},
						subtitle: {text: 'данные загружены из локальной базы данных'},
						xAxis: {
							type: 'datetime',
							dateTimeLabelFormats: { // don't display the dummy year
								month: '%e. %b',
								year: '%b'
							}
						},
						yAxis: {
							title: {text: tag_rus_short[tag]},
							min: 0
						},
						tooltip: {
							formatter: function() {
									return '<b>'+ this.series.name +'</b><br/>'+
									Highcharts.dateFormat('%e. %b', this.x) +':'+tag_rus_short[tag]+' '+ this.y +'';
							}
						},
						series: [{
							name: tag_rus_short[tag],
							data: result_data
						}]
					});
				}, function (tx,e) {
					error_notice('Ошибка при выборке из базы:'+ tx + " код " +e.code + ": " + e.message);
				});

        });
    }

    function getUrlVars(href) {
        var vars = [],
            hash;
		//if(href==undefined) return undefined;
        var hashes = href.slice(href.indexOf('?') + 1).split('&');
        for (var i = 0; i < hashes.length; i++) {
            hash = hashes[i].split('=');
            vars.push(hash[0]);
            vars[hash[0]] = hash[1];
        }
        if (vars["p"] == undefined) {
            vars = [];
            var pageName = href.slice(href.lastIndexOf('/') + 1);
            vars.push(pageName);
            vars["p"] = pageName;
        }
        return vars;
    }


    function insert_history_manager_training_form1(page)
	{
        notice('inserting manager_training_form1');
        try {
            if (window.openDatabase) {
                //db = openDatabase(dbName, dbVersion, dbDiscription, dbEstimatedSize);
                if (db)
				{
                    var trs = $(page).find("table tbody tr");
                    notice("найдено " + (trs.length - 4) + " игроков");
					if(trs.length<22)
						return 0;
                    var queryArray = [],
                        query = " ";
                    for (var i = 0; i < trs.length; i++) {
                        if ($(trs[i]).attr("class") != "caption") if ($(trs[i]).attr("class") != "thead") {
                            var tds = $(trs[i]).find("td");
                            var player_id = getUrlVars($(tds[0]).find("a")[0].href)["id"].replace('#', '');
                            var name = ($(tds[0]).find("a").text());
                            var age = $(tds[1]).text();
                            var goa = $(tds[2]).text();
                            var def = $(tds[3]).text();
                            var att = $(tds[4]).text();
                            var sho = $(tds[5]).text();
                            var pas = $(tds[6]).text();
                            var spe = $(tds[7]).text();
                            var str = $(tds[8]).text();
                            var sc = $(tds[9]).text();
                            var ene = $(tds[10]).text();
                            var form = $(tds[11]).text();
                            var tra = $(tds[13]).text();
                            var schedule = $(tds[14].innerHTML).find("option[selected]").text();

                            if (tds[15] != undefined) var pos = tds[15].innerHTML;
                            else var pos = "запас";
                            tds = [];

                            queryArray.push(
							[player_id, //номер игрока
                            name, //имя
                            age, //возраст
                            goa, //вратарь
                            def, //защита
                            att, //атака
                            sho, //бросок
                            pas, //пас
                            spe, //скорость
                            str, //сила
                            sc, //СОб
                            ene, //енергия
                            form, //форма
                            tra, //треньки
                            schedule, //расписание
                            pos //позиция
                            ]);
                        }
                    }
					db.transaction(function (tx,e)
					{
						for (var i = 0; i < queryArray.length; i++) {
							if ((queryArray[i]) != undefined)
								tx.executeSql("INSERT INTO history_manager_training_form1 VALUES(NULL, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, datetime('now'));", queryArray[i], function (tx, result)
								{
								}, function (tx,e) {
									error_notice('Ошибка при вставке в базу:'+ tx + " код " +e.code + ": " + e.message);
									});
						}
						insert_history_daily(tx,"history_manager_training_form1");
					});
				} else {error_notice('Ошибка при открытии базы');}
            } else {error_notice('Web Databases не поддерживается');}
        } catch (e) {error_notice('Ошибка! '+e.name + ": " + e.message);}
    }

    function insert_history_manager_team_players(page) {
        notice('inserting manager_team_players');
        try {
			if (window.openDatabase) {
				//db = openDatabase(dbName, dbVersion, dbDiscription, dbEstimatedSize);
				if (db) {
					var trs = $(page).find("table tbody tr[class]");
					notice("найдено " + (trs.length - 2) + " игроков");
					if(trs.length<22)
						return 0;
					var queryArray = [],
						query = " ";
					for (var i = 0; i < trs.length; i++) {
						if ($(trs[i]).attr("class") != "caption") if ($(trs[i]).attr("class") != "thead") {
							var tds = $(trs[i]).find("td");
							var player_id = getUrlVars($(tds[0]).find("a")[0].href)["player_id"].replace('#', '');
							var name = ($(tds[0]).find("a").text());
							var age = $(tds[1]).text();
							var perf = $.trim($(tds[2]).text());
							var perfq = $(tds[2]).find(Qimg).length;
							var pot = $.trim($(tds[3]).text());
							var potq = $(tds[3]).find(Qimg).length;
							var skill = $(tds[13]).text();
							var ai = $(tds[14]).text();
							var wit = $(tds[15]).text();
							tds = [];

							queryArray.push(
							[player_id, //номер игрока
							name,//имя
							age, //возраст
							perf, //работоспособность
							perfq, //работоспособность приблизительна(0) или точна(1)
							pot, potq, skill, ai, wit
							]);
						}
					}
					db.transaction(function (tx,e)
					{
						for (var i = 0; i < queryArray.length; i++) {
							if ((queryArray[i]) != undefined)
								tx.executeSql("INSERT INTO history_manager_team_players VALUES(NULL, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, datetime('now'));", queryArray[i], function (tx, result)
								{
								}, function (tx,e) {
									error_notice('Ошибка при вставке в базу:'+ tx + " код " +e.code + ": " + e.message);
									});
						}
						insert_history_daily(tx,"history_manager_team_players");
					});
				} else {error_notice('Ошибка при открытии базы');}
            } else {error_notice('Web Databases не поддерживается');}
        } catch (e) {error_notice('Ошибка! '+e.name + ": " + e.message);}
    }
	
	function insert_history_manager_team_contracts(page)
	{
        notice('inserting manager_team_contracts');
        try {
			if (window.openDatabase)
			{
				//db = openDatabase(dbName, dbVersion, dbDiscription, dbEstimatedSize);
				if (db) {
					var trs = $(page).find("table tbody tr[class]");
					notice("найдено " + (trs.length - 2) + " игроков");
					if(trs.length<22)
						return 0;
					var queryArray = [],
						query = " ";
					for (var i = 0; i < trs.length; i++) {
						if ($(trs[i]).attr("class") != "caption") if ($(trs[i]).attr("class") != "thead") {
							var tds = $(trs[i]).find("td");
							var player_id = getUrlVars($(tds[0]).find("a")[0].href)["id"].replace('#', '');
							var name = ($(tds[0]).find("a").text());
							var age = $(tds[1]).text();
							var contract = $(tds[2]).text();
							var salary = $(tds[3]).text();
							var perf = $.trim($(tds[4]).text());
							var perfq = $(tds[4]).find(Qimg).length;
							var pot = $.trim($(tds[5]).text());
							var potq = $(tds[5]).find(Qimg).length;
							var loy = parseInt($(tds[6],10).text())
							if(isNaN(loy))
								loy = 0;
							var countryShort = $(tds[7]).text();
							var ai = $(tds[8]).text();
							tds = [];

							queryArray.push(
							[player_id, //номер игрока
							name,//имя
							age, //возраст
							contract, //срок контракта
							salary, //зарплата
							perf, //работоспособность
							perfq, //работоспособность приблизительна(0) или точна(1)
							pot, potq, loy, countryShort, ai
							]);
						}
					}
					db.transaction(function (tx,e)
					{
						for (var i = 0; i < queryArray.length; i++) {
							if ((queryArray[i]) != undefined)
								tx.executeSql("INSERT INTO history_manager_team_contracts VALUES(NULL, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, datetime('now'));", queryArray[i], function (tx, result)
								{
								}, function (tx,e) {
									error_notice('Ошибка при вставке в базу:'+ tx + " код " +e.code + ": " + e.message);
									});
						}
						insert_history_daily(tx,"history_manager_team_contracts");
					});
				} else {error_notice('Ошибка при открытии базы');}
            } else {error_notice('Web Databases не поддерживается');}
        } catch (e) {error_notice('Ошибка! '+e.name + ": " + e.message);}
    }

	function insert_history_manager_player_market_sql(page)
	{
        notice('Обработка страницы трансферного рынка');
        try {
			if (window.openDatabase) {
				//db = openDatabase(dbName, dbVersion, dbDiscription, dbEstimatedSize);
				if (db) {
					var trs = $(page).find("table tbody tr[class]");
					notice("найдено " + (trs.length - 2) + " игроков");
					var queryArray = [],
						query = " ";
					for (var i = 0; i < trs.length; i++) {
						if ($(trs[i]).attr("class") != "caption")
						  if ($(trs[i]).attr("class") != "ysptblthbody1") {
							var tds = $(trs[i]).find("td");
							var player_id = getUrlVars($(tds[0]).find("a")[0].href)["id"].replace('#', '');
                            var name =$(tds[0]).find("a").text();
                            var age = $.trim($(tds[1]).text());
                            var goa = $.trim($(tds[2]).text());
                            var def = $.trim($(tds[3]).text());
                            var att = $.trim($(tds[4]).text());
                            var sho = $.trim($(tds[5]).text());
                            var pas = $.trim($(tds[6]).text());
                            var spe = $.trim($(tds[7]).text());
                            var str = $.trim($(tds[8]).text());
                            var sc = $.trim($(tds[9]).text());
                            var ai = $.trim($(tds[10]).text());
                            var skill = $.trim($(tds[11]).text());
							var perf = $.trim($(tds[12]).text());
							var perfq = $(tds[12]).find(Qimg).length;
							var pot = $.trim($(tds[13]).text());
							var potq = $(tds[13]).find(Qimg).length;
							var contract = $.trim($(tds[14]).text());
							var nat = hname($(tds[15]).find("img").attr("src"));
							var price = $.trim($(tds[16]).text()).replace(" ","").replace(" ","").replace(" ","").replace(" ","").replace(" ","");
								var timedate = $.trim($(tds[17]).text());
								var hour = timedate.substr(0,timedate.indexOf(":"));
								var min = timedate.substr(timedate.indexOf(":")+1,timedate.indexOf(",")-timedate.indexOf(":")-1);
								var day = timedate.substr(timedate.indexOf(",")+2,timedate.indexOf(".")-timedate.indexOf(",")-2);
								var mon = timedate.substr(timedate.indexOf(".")+1,2);
							var datetime = "2012-"+mon+"-"+day+" "+hour+":"+min;
							tds = [];

							queryArray.push(
							[player_id, //номер игрока
							name,//имя
							age, //возраст
                            goa, //вратарь
                            def, //защита
                            att, //атака
                            sho, //бросок
                            pas, //пас
                            spe, //скорость
                            str, //сила
                            sc, //СОб
							ai,
							skill,
							perf,
							perfq, //работоспособность приблизительна(0) или точна(1)
							pot,
							potq,
							contract,
							nat,
							price,
							datetime
							]);
						}
					}
					db.transaction(function (tx,e)
					{
						for (var i = 0; i < queryArray.length; i++) {
							if ((queryArray[i]) != undefined)
								tx.executeSql("INSERT INTO history_manager_player_market_sql VALUES(NULL, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, datetime('now'));", queryArray[i], function (tx, result)
								{
								}, function (tx,e) {
									error_notice('Ошибка при вставке в базу:'+ tx + " код " +e.code + ": " + e.message);
									});
						}
					});
				} else {error_notice('Ошибка при открытии базы');}
            } else {error_notice('Web Databases не поддерживается');}
        } catch (e) {error_notice('Ошибка! '+e.name + ": " + e.message);}
    }

	function insert_history_public_team_info_players(page)
	{
        //notice("Обработка публичной таблицы игроков команды");
		var team_id=getUrlVars($(page).find("div a").attr("href"))["team_id"]
		var trs = $(page).find("table tbody tr[class=sr1],table tbody tr[class=sr2]");
		var top_tr = $(page).find("table tbody tr[class=darkbg]");
		var team_name = ($.trim($(top_tr).text()).split(",")[0].split("игроках ")[1]);
		if((trs.length<20)||(trs.length>55))
		{
			error_notice("Найдено " + (trs.length - 1) + " игроков в команде "+team_name + " id="+team_id);
			return 0;
		}
		if(undefined==team_id)
		{
			error_notice("team_id == undefined");
			return 0;
		}
		var player_id_arr = [];
		notice("Найдено " + (trs.length - 1) + " игроков в команде "+team_name + " id="+team_id);
		var page_name_list=[], url_list=[];
		for (var i = 0; i < trs.length; i++)
		{
			var tds = $(trs[i]).find("td");
			var undef = $(tds[0]).find("a")[0];
			if (undef!=undefined)
			{
				var player_id = getUrlVars(undef.href)["id"];
				if(undefined==player_id)
				{
					error_notice("player_id == undefined");
					return 0;
				}else player_id_arr.push(player_id);
				
				//teamIdTmp = getUrlVars(window.location.href)["team_id"];
				var href = "http://www.hockeyarena.net/ru/index.php?p=public_player_info.inc&id="+player_id;
				page_name_list.push("history_public_player_info_"+player_id);
				url_list.push(href);
			}
		}
		//var def = 
		download_list(page_name_list,url_list);
		//def.done(function (result)
		{
			try
			{
				if (window.openDatabase)
				{
					//db = openDatabase(dbName, dbVersion, dbDiscription, dbEstimatedSize);
					if (db)
					{
						var query = 'SELECT count(*) as c from history_daily where "page_name" in ('
						for (var i = 0; i < player_id_arr.length-1; i++)
							query = query+'"history_public_player_info_'+player_id_arr[i]+'", ';
						query = query+'"history_public_player_info_'+player_id_arr[player_id_arr.length-1]+'") and date(add_date_time,"+7 hour","-30 minute")>=date(datetime("now","+7 hour")) group by page_name';
						//error_notice(query);
						db.transaction(function (tx,e)
						{
							tx.executeSql(query, [], function (tx, results)
							{
								//error_notice();
								if(results.rows.length<player_id_arr.length)
									notice("Не полная информация по команде "+team_name + " id="+team_id+", в составе игроков "+player_id_arr.length+", но в базе " + results.rows.length + " игроков. Повторите.",10000);
								else 
								{
									db.transaction(function (tx,e)
									{
										insert_history_daily(tx,"history_public_team_info_players_"+team_id);
									});
								}
							}, function (tx,e) {
								error_notice('Ошибка при выборке из базы:'+ tx + " код " +e.code + ": " + e.message);
							});
						});
					} else {error_notice('Ошибка при открытии базы');}
				} else {error_notice('Web Databases не поддерживается');}
			} catch (e) {error_notice('Ошибка! '+e.name + ": " + e.message);}
		}
		//);
		//return def;
    }
	
	function insert_history_public_league_standings(page)
	{
        //notice("Обработка публичной таблицы команд");
		var trs = $(page).find("table tbody tr[class=sr1],table tbody tr[class=sr2]");
		//notice("Найдено " + (trs.length - 4) + " команд");
		//if(false)
		var team_id_arr = [];
		for (var i = 0; i < trs.length; i++)
		{
			var team_id = getUrlVars($(trs[i]).find("a")[0].href)["team_id"];
			if(undefined==team_id)
			{
				error_notice("team_id == undefined");
				return 0;
			}else team_id_arr.push(team_id);
			var href = "http://www.hockeyarena.net/ru/index.php?p=public_team_info_players.php&team_id="+team_id;
			dbManager.download("history_public_team_info_players_"+team_id,href,undefined);
		}
 		try {
			if (window.openDatabase) {
				//db = openDatabase(dbName, dbVersion, dbDiscription, dbEstimatedSize);
				if (db) {
					//var trs = $(page).find("table tbody tr[class=sr1],table tbody tr[class=sr2]");
					var league_name = $($(page).find("option[selected]")[0]).text().substr(0,2)+";"+$($(page).find("option[selected]")[1]).attr("value")+";"+$($(page).find("option[selected]")[2]).attr("value");
                    notice("Найдено " + (trs.length) + " команд в лиге " + league_name);
					if(trs.length<16)
					{
						error_notice("Команд меньше 16-и");
						return 0;
					}
                    var queryArray = [],
                        query = " ";
                    for (var i = 0; i < trs.length; i++) 
					{
                        if ($(trs[i]).attr("class") != "caption") if ($(trs[i]).attr("class") != "thead") 
						{
                           var tds = $(trs[i]).find("td");
							var position = $(tds[0]).text().trim().split(".")[0];
							var team_id = getUrlVars($(tds[0]).children().attr("href"))["team_id"];
							var team_name = $(tds[0]).children().text();
							var mat = $(tds[1]).text();
							var win = $(tds[2]).text();
							var drw = $(tds[3]).text();
							var lose =$(tds[4]).text();
							var wo =  $(tds[5]).text();
							var lo =  $(tds[6]).text();
							var pts = $(tds[7]).text();
							var g_p = $(tds[8]).text().split('/')[0];
							var g_n = $(tds[8]).text().split('/')[1];
							var pn =  $(tds[9]).text();
							var home =$(tds[10]).text();
							var away =$(tds[11]).text().trim();
														tds = [];
							// 
							queryArray.push(
							[league_name, position, team_id, team_name, mat, win, drw, lose, wo, lo, pts, g_p, g_n, pn, home, away]
							);
						}
					}

					var query = 'SELECT count(*) as c from history_daily where "page_name" in ('
					for (var i = 0; i < team_id_arr.length-1; i++)
						query = query+'"history_public_team_info_players_'+team_id_arr[i]+'", ';
					query = query+'"history_public_team_info_players_'+team_id_arr[team_id_arr.length-1]+'") and date(add_date_time,"+7 hour","-30 minute")>=date(datetime("now","+7 hour")) group by page_name';
					//error_notice(query);
					db.transaction(function (tx,e)
					{
						tx.executeSql(query, [], function (tx, results)
						{
							//error_notice();
							if (results.rows.length<team_id_arr.length)
								notice("Не полная информация по лиге "+ league_name +", в составе команд "+ team_id_arr.length +", но в базе " + results.rows.length + ". Повторите.",10000);
							else
							{
								db.transaction(function (tx,e)
								{
 									for (var i = 0; i < queryArray.length; i++) 
									{
										if ((queryArray[i]) != undefined)
											tx.executeSql("INSERT INTO history_public_league_standings VALUES(NULL, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, datetime('now'));", queryArray[i], function (tx, result)
											{
											}, function (tx,e) {
												error_notice('Ошибка при вставке в базу:'+ tx + " код " +e.code + ": " + e.message);
												});
									}
									insert_history_daily(tx,"history_public_league_standings_"+ league_name);
								});
							}
						}, function (tx,e) {
							error_notice('Ошибка при выборке из базы:'+ tx + " код " +e.code + ": " + e.message);
						});
					});

				} else {error_notice('Ошибка при открытии базы');}
            } else {error_notice('Web Databases не поддерживается');}
        } catch (e) {error_notice('Ошибка! '+e.name + ": " + e.message);} 
    }
	
	function insert_history_manager_transfer_list_sql(page,day_str)
	{
        notice('inserting insert_history_manager_transfer_list_sql');
        try
		{
			if (window.openDatabase)
			{
				//db = openDatabase(dbName, dbVersion, dbDiscription, dbEstimatedSize);
				if (db) {
					var trs = $(page).find("table tbody tr[class]");
					notice("найдено " + (trs.length - 1) + " игроков");
					if(trs.length<22)
						return 0;
					var queryArray = [],
						query = " ";
					for (var i = 0; i < trs.length; i++) {
						if ($(trs[i]).attr("class") != "caption")
						{
							var tds = $(trs[i]).find("td");
							var day = $(tds[0]).text();
							var price = $(tds[1]).text().replace(" ","").replace(" ","").replace(" ","").replace(" ","").replace(" ","").replace(" ","");
							var team_from_name	=		$.trim($($(tds[2]).find("a")[1]).text());
							var team_from_id	=	getUrlVars($($(tds[2]).find("a")[1]).attr("href"))["team_id"]
							var team_to_name	=		$.trim($($(tds[3]).find("a")[1]).text());
							var team_to_id		=	getUrlVars($($(tds[3]).find("a")[1]).attr("href"))["team_id"]
							var man_from_name	=		$.trim($($(tds[2]).find("a")[0]).text());
							var man_from_id		=	getUrlVars($($(tds[2]).find("a")[0]).attr("href"))["id"];
							var man_to_name		=		$.trim($($(tds[3]).find("a")[0]).text());
							var man_to_id		=	getUrlVars($($(tds[3]).find("a")[0]).attr("href"))["id"];
							var name 			=		$.trim($($(tds[4]).find("a")[0]).text());
							var player_id		=	getUrlVars($($(tds[4]).find("a")[0]).attr("href"))["id"];
							tds = [];

							queryArray.push(
							[player_id, //номер игрока
							name,//имя
							day,
							price,
							team_from_name,
							team_from_id,
							man_from_name,
							man_from_id,
							team_to_name,
							team_to_id,
							man_to_name,
							man_to_id
							]);
						}
					}
					db.transaction(function (tx,e)
					{
						for (var i = 0; i < queryArray.length; i++)
						{
							if ((queryArray[i]) != undefined)
							{
								tx.executeSql("INSERT INTO history_manager_transfer_list_sql VALUES(NULL, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, datetime('now'));", queryArray[i], function (tx, result)
								{
									//
								}, function (tx,e)
								{
									error_notice('Ошибка при вставке в базу:'+ tx + " код " +e.code + ": " + e.message);
								});
							}
							else error_notice("Ошибка : queryArray == undefined");
						}
					});
				} else {error_notice('Ошибка при открытии базы');}
            } else {error_notice('Web Databases не поддерживается');}
        } catch (e) {error_notice('Ошибка! '+e.name + ": " + e.message);}
    }
	
	//процедура вставки метки
	function insert_history_daily(tx,pageName)
	{
		if (pageName != undefined)
			tx.executeSql("INSERT INTO history_daily VALUES(NULL, ?, datetime('now'));", [pageName], function (tx, result)
			{
				notice("Информация "+pageName+" сохранена",10000);
			}, function (tx,e) {
				error_notice('Ошибка при вставке в базу:'+ tx + " код " +e.code + ": " + e.message);
				});
    }
	
	//процедура определяющая в какую процедуру передать данные после успешной загрузки
	function success_download(page_name,page)
	{
		if("history_manager_training_form1"==page_name)
			insert_history_manager_training_form1(page);
		if("history_manager_team_players"==page_name)
			insert_history_manager_team_players(page);
		if("history_manager_team_contracts"==page_name)
			insert_history_manager_team_contracts(page);
		if("history_public_team_info_players"==page_name.substr(0,page_name.lastIndexOf("_")))
		{
			var team_id = page_name.split("_")[page_name.split("_").length-1];
			insert_history_public_team_info_players(page,team_id);
		}
		if("history_public_league_standings"==page_name.substr(0,page_name.lastIndexOf("_")))
		{
			//var league_name  = page_name.split("_")[page_name.split("_").length-1];
			insert_history_public_league_standings(page);
		}
 		if("history_public_player_info"==page_name.substr(0,page_name.lastIndexOf("_")))
		{
			var player_id  = page_name.split("_")[page_name.split("_").length-1];
			insert_history_public_player_info(page,player_id);
		}
    }
	
	//процедура, после проверки на повтор, скачивает данные и анализирует их.
	//Если url не определён, то читает данные из page
	function download(page_name, url, page)
	{
		try
		{
			if (window.openDatabase)
			{
				//db = openDatabase(dbName, dbVersion, dbDiscription, dbEstimatedSize);
				if (db)
				{
					db.transaction(function (tx,e) 
					{
						tx.executeSql('SELECT count(*) as c from history_daily where page_name=? and date(add_date_time,"+7 hour","-30 minute")>=date(datetime("now","+7 hour"));', [page_name], function (tx, results)
						{
							var result = false;
							if (results.rows && results.rows.length)
							{
								if(results.rows.item(0).c<1)
									result = true;
								if(!result)
									notice("Повтор "+page_name+",\n сегодня эти данные уже собирались");
								//else
								//	notice("Сбор "+page_name+" разрешён");
								if(result)//true - разрешаю вставлять данные
								{
									if(undefined!=url)
									{
										$.ajax(
										{
											type: "GET",
											url: url,
											//async: false,
											success:function (response){success_download(page_name,$(response).find("div.clearfix").html());}
										});
									}else{success_download(page_name,page);}
								}
							}
						}, function (tx,e) {error_notice('Ошибка при выборке из базы:'+ tx + " код " +e.code + ": " + e.message);});
					});
				} else {error_notice('Ошибка при открытии базы');}
            } else {error_notice('Web Databases не поддерживается');}
        } catch (e) {error_notice('Ошибка! '+e.name + ": " + e.message);}
    }
	
	//то же что и download только для списка
	function download_list(page_name_list, url_list)
	{
		var deferred = jQuery.Deferred();
		try
		{
			if (window.openDatabase)
			{
				//db = openDatabase(dbName, dbVersion, dbDiscription, dbEstimatedSize);
				if (db)
				{
					db.transaction(function (tx,e) 
					{
						for (var i = 0; i < page_name_list.length; i++)
						{
							(function (index, tx,e) 
							{
								tx.executeSql('SELECT count(*) as c,page_name from history_daily where page_name=? and date(add_date_time,"+7 hour","-30 minute")>=date(datetime("now","+7 hour"));', [page_name_list[index]], function (tx, results)
								{
									var result = false;
									if (results.rows && results.rows.length)
									{
										if(results.rows.item(0).c<1)
											result = true;
										if(!result)
											notice("Повтор "+page_name_list[index]+",\n сегодня эти данные уже собирались");
										//else
										//	notice("Сбор "+page_name+" разрешён");
										if(result)//true - разрешаю вставлять данные
										{
											//if(undefined!=url)
											{
												$.ajax(
												{
													type: "GET",
													url: url_list[index],
													//async: false,
													success:function (response)
													{
														success_download(page_name_list[index],$(response).find("div.clearfix").html());
													}
												});
											}//else{success_download(page_name_list[index],page);}
										}
									}
									if((page_name_list.length-1)==index)
									{
										// setInterval(function() 
										// {
											deferred.resolve("ready");
										//}, 3000);
									}
								}, function (tx,e) {error_notice('Ошибка при выборке из базы:'+ tx + " код " +e.code + ": " + e.message);});
							})(i,tx,e)
						}
					});
				} else {error_notice('Ошибка при открытии базы');}
            } else {error_notice('Web Databases не поддерживается');}
        } catch (e) {error_notice('Ошибка! '+e.name + ": " + e.message);}
		return deferred;
    }
	
    return {
        latest: function () 
		{
            return latest;
        },

        //создание таблиц если не сущуствуют
        init: create_tables,
        //вставка публичной информации о игроках
        insert_history_public_player_info: insert_history_public_player_info,
        //выборка публичной информации о игроках
        selectPlayerPublic: selectPlayerPublic,
        //вставка тренировок игроков
        insert_history_manager_training_form1: insert_history_manager_training_form1,
		//игроки -> атрибуты
        insert_history_manager_team_players: insert_history_manager_team_players,
		//игроки -> контракты
		insert_history_manager_team_contracts:insert_history_manager_team_contracts,
		//игроки -> трансфер -> выполнить
		insert_history_manager_player_market_sql:insert_history_manager_player_market_sql,
		//игроки -> трансфер -> список последних трансферов
		insert_history_manager_transfer_list_sql:insert_history_manager_transfer_list_sql,
		//публичная таблица игроков команды
		insert_history_public_team_info_players:insert_history_public_team_info_players,
		//публичная таблица команд лиги
		insert_history_public_league_standings:insert_history_public_league_standings,
		//процедура, после проверки на повтор, скачивает данные и анализирует их.
		//Если url не определён, то читает данные из третьего аргумента
		download:download,
		//возвращает ассоциативный массив get-параметров url
		getUrlVars:getUrlVars,
		select_highcharts_public_player_info:select_highcharts_public_player_info,
		download_list:download_list,
		notice:notice
    };

})('rem', '#players_tbody');